<?php
session_start();
include('../db_connect.php');
include 'fun_admin.php';

if (!isset($_SESSION['us_name']))
    {
    echo 'Необходима авторизации';
    exit();
    }

if(isset($_POST))
{
    $pdo = get_pdo_connection();

    /*обновляем или создаем пост*/
    $Header     = $_POST['Header'];
    $URL        = $_POST['URL'];
    $Descr      = $_POST['Descr'];
    if (trim($URL) <> '')
    {
        $pref = get_const('pref_st');
        $URL = $pref . trim($URL);
    }
    $Content    = $_POST['Content'];
    $ID         = $_POST['ID'];

    if($_POST['ID'] !== '')
    {
        $text = "UPDATE `posts` SET "
                    . "`Header`     = :par_Header,"
                    . "`URL`        = :par_URL,"
                    . "`Descr`      = :par_Descr,"
                    . "`Content`    = :par_Content" .
                " WHERE `ID` = :par_ID";

        $params = array(
            'par_Header'    => $Header,
            'par_URL'       => $URL,
            'par_Descr'     => $Descr,
            'par_Content'   => $Content,
            'par_ID'        => $ID,
            );
    }
    else
    {
        $text = "INSERT INTO `posts`(`Header`, `URL`, `Descr`, `Content`) VALUES(:par_Header, :par_URL, :par_Descr, :par_Content)";

        $params = array(
            'par_Header'    => $Header,
            'par_URL'       => $URL,
            'par_Descr'     => $Descr,
            'par_Content'   => $Content,
            );
    }

    $result = $pdo -> prepare($text);
    $result -> execute($params);

    unset($params);

    /*Привязываем пост к рубрике*/
    if(isset($_POST['RubricID']))
    {
        $RubricID   = $_POST['RubricID'];

        if($_POST['ID'] !== '')
        {
            $text = "
            DELETE FROM `post_in_rubric` WHERE `Post_ID` = :par_ID";

            $params = array(
                'par_ID'    => $ID,
                );

            $result = $pdo -> prepare($text);
            $result -> execute($params);

            $text = "
            INSERT INTO `post_in_rubric`(`Post_ID`, `Rubric_ID`) VALUES (:par_ID, :par_RubricID)";

            $params['par_RubricID'] = $RubricID;

            $result = $pdo -> prepare($text);
            $result -> execute($params);

        }
        else
        {
            $text   = "SELECT MAX(`ID`) AS MaxID FROM `posts`";

            $result = $pdo -> query($text);
            while ($row = $result -> fetch(PDO::FETCH_ASSOC))
            {
                $max_ID = $row['MaxID'];
            }

            $text = "
            INSERT INTO `post_in_rubric`(`Post_ID`, `Rubric_ID`) VALUES (:par_max_ID, :par_RubricID)";

            $params = array(
                'par_max_ID'    => $max_ID,
                'par_RubricID'  => $RubricID,
                );

            $result = $pdo -> prepare($text);
            $result -> execute($params);

        }
    }

    $pdo = NULL;
}
?>

<META HTTP-EQUIV="Refresh" CONTENT="0; URL=admin.php?TypeWin=ArtList">
