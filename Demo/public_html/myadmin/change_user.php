<?php
session_start();
include('../db_connect.php');
include 'fun_admin.php';

if (!isset($_SESSION['us_name']))
    {
    echo 'Необходима авторизации';
    exit();
    }

if(isset($_POST))
{
    $pdo = get_pdo_connection();

    /*обновляем или создаем пользователя*/
    $login  = $_POST['Login'];
    $pass   = trim($_POST['Pass']);
    $ID     = $_POST['ID'];
    $hash   = password_hash($pass, PASSWORD_DEFAULT);

    if($ID !== '' And $ID !== NULL)
    {
        $text = "UPDATE `users` SET
                    `Login`     = :par_Login,
                    `Password`  = :par_Pass,
                    `PasHash`    = :par_hash
                WHERE `ID` = :par_ID";

        $params = array('par_ID'        => $ID
                        ,'par_Login'    => $login
                        ,'par_Pass'     => $pass
                        ,'par_hash'     => $hash
                        );
    }
    else
    {
        $text = "INSERT INTO `users`(`Login`, `Password`, `PasHash`) VALUES(:par_Login, :par_Pass, :par_hash)";

        $params = array('par_Login'    => $login
                        ,'par_Pass'     => $pass
                        ,'par_hash'     => $hash
                       );
    }

    $result = $pdo -> prepare($text);
    $result -> execute($params);

    unset($params);
    $pdo = NULL;
}
?>

<META HTTP-EQUIV="Refresh" CONTENT="0; URL=admin.php?TypeWin=Users">
