<?php

include('../db_connect.php');

if (isset($_POST))
{
    $text   = "SELECT `Name` FROM `Const`";

    $pdo = get_pdo_connection();
    $result = $pdo->query($text);
    while ($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        $name       = $row['Name'];

        if (array_key_exists($name, $_POST))
        {
            $new_name   = 'new_' . $row['Name'];
            $val        = trim($_POST[$name]);
            $new_val    = trim($_POST[$new_name]);

            if ($new_val !== '' and $val !== $new_val)
            {
                $text = "UPDATE `Const` SET `Value` = :par_Value WHERE `Name` = :par_Name";
                $params = array(
                    'par_Value' => $new_val,
                    'par_Name'  => $name
                    );

                $result = $pdo -> prepare($text);
                $result -> execute($params);

            }
        }
    }

    $pdo = NULL;
}
?>
<META HTTP-EQUIV="Refresh" CONTENT="0; URL=admin.php?TypeWin=Const">

