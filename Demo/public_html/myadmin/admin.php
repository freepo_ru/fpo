<?php
session_start();
include '../db_connect.php';
include 'fun_admin.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $Password   = trim($_POST['Pass']);
    $UserName   = $_POST['UserName'];
    $auth       = FALSE;

    $text = "SELECT `PasHash` FROM `users` WHERE `Login` = :par_UserName";

    $params = array('par_UserName'  => $UserName);

    $pdo = get_pdo_connection();
    $result = $pdo -> prepare($text);
    $result -> execute($params);

    while ($row = $result -> fetch(PDO::FETCH_ASSOC))
    {
        $hash = trim($row['PasHash']);
        if (password_verify($Password, $hash))
        {
            $auth = TRUE;
            $_SESSION['us_name'] = $UserName;
        }
    }

    $pdo = NULL;

    if ($auth === FALSE)
    {
        echo 'Ошибка авторизации';
        exit();
    }

}

if (isset($_GET['TypeWin']))
{
    $TypeMainWin = $_GET['TypeWin'];
}
else
{
    $TypeMainWin = "Nothing";
}

if (isset($_GET['ID']))
{
    $ChangeID = $_GET['ID'];
}
else
{
    $ChangeID = NULL;
}

if (!isset($_SESSION['us_name']))
    {
    echo 'Необходима авторизации';
    exit();
    }
?>

<!DOCTYPE html PUBLIC -//W3C//DTD XHTML 1.0 Strict//EN http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd>
<!--<html lang="en">-->
<html>

    <head>
        <title>Администрирование</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="styleadmin.css">
    </head>

    <body>
        <div id = DivMain>
            <div id = DivHeader>
                УПРАВЛЕНИЕ САЙТОМ
            </div>
            <div  class = "row_dir">
                <div id = DivLeft>
                    <p>
                        <a href="/myadmin/admin.php?TypeWin=<?php echo 'Main'?>">Главная</a>
                    </p>
                    <p>
                        <a href="/myadmin/admin.php?TypeWin=<?php echo 'ArtList'?>">Записи</a>
                    </p>
                    <p>
                        <a href="/myadmin/admin.php?TypeWin=<?php echo 'BlockList'?>">Рубрики</a>
                    </p>
                    <p>
                        <a href="/myadmin/admin.php?TypeWin=<?php echo 'Const'?>">Настройки</a>
                    </p>
                    <p>
                        <a href="/myadmin/admin.php?TypeWin=<?php echo 'Users'?>">Пользователи</a>
                    </p>
                </div>
                <div id = DivCenter>
                    <p>
                        <?php GetMainWindow($TypeMainWin, $ChangeID);?>
                    </p>
                </div>
            </div>
        </div>
    </body>

</html>
<?php

?>
