<?php
session_start();
include('../db_connect.php');
include 'fun_admin.php';

if (!isset($_SESSION['us_name']))
    {
    echo 'Необходима авторизации';
    exit();
    }

if(isset($_POST))
{
    $level      = get_hierarhy_level($_POST['ParentID']);
    $BlockName  = $_POST['BlockName'];
    $ParentID   = $_POST['ParentID'];
    $ID         = $_POST['ID'];

    if($ID !== '')
    {

        $text = "UPDATE `rubrics` SET `Name` = :par_BlockName,"
                                    . "`Parent_ID` =  :par_ParentID,"
                                    . "`Level` =  :par_level" .
                " WHERE `ID` = :par_ID";

        $params = array(
            'par_level'     => $level,
            'par_BlockName' => $BlockName,
            'par_ParentID'  => $ParentID,
            'par_ID'        => $ID,
            );
    }
    else
    {
        $text = "INSERT INTO `rubrics`(`Name`, `Parent_ID`, `Level`) VALUES(:par_BlockName, :par_ParentID, :par_level)";

        $params = array(
            'par_BlockName' => $BlockName,
            'par_ParentID'  => $ParentID,
            'par_level'     => $level,
            );
    }

    $pdo = get_pdo_connection();
    $result = $pdo -> prepare($text);
    $result -> execute($params);

    $pdo = NULL;

}
?>

<META HTTP-EQUIV="Refresh" CONTENT="0; URL=admin.php?TypeWin=BlockList">
