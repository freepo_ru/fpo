<?php
session_start();
include('../db_connect.php');

if (!isset($_SESSION['us_name']))
    {
    echo 'Необходима авторизации';
    exit();
    }

if(isset($_GET))
{
    $par_TableName  = $_GET['TableName'];
    $par_ID         = $_GET['ID'];

    if($par_TableName == 'rubrics')
    {
        $text = "DELETE FROM `rubrics` WHERE ID = :par_ID";
    }
    elseif ($par_TableName == 'posts')
    {
        $text = "DELETE FROM `posts` WHERE ID = :par_ID";
    }
    elseif ($par_TableName == 'users')
    {
        $text = "DELETE FROM `users` WHERE ID = :par_ID";
    }

    $pdo = get_pdo_connection();
    $result = $pdo -> prepare($text);
    $result -> execute(array('par_ID' => $par_ID));

    $pdo = NULL;

}

if($_GET['TableName'] == 'rubrics')
{
    echo '<META HTTP-EQUIV="Refresh" CONTENT="0; URL=admin.php?TypeWin=BlockList">';
}
elseif ($_GET['TableName'] == 'posts')
{
    echo '<META HTTP-EQUIV="Refresh" CONTENT="0; URL=admin.php?TypeWin=ArtList">';
}
elseif ($_GET['TableName'] == 'users')
{
    echo '<META HTTP-EQUIV="Refresh" CONTENT="0; URL=admin.php?TypeWin=Users">';
}
?>
