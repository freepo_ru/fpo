<?php
function get_const($par_name)
{

    $val    = '';

    $text   = "SELECT `Value` FROM `Const` WHERE `Name` = :par_name";
    $pdo    = get_pdo_connection();
    $params = array('par_name' => $par_name);
    $result = $pdo -> prepare($text);
    $result -> execute($params);

    while ($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        $val = $row['Value'];
    }
    $pdo = NULL;

    return trim($val);
}

function get_hierarhy_level($par_ParentID)
{
    if($par_ParentID == NULL or $par_ParentID == 0 or $par_ParentID == '')
    {
        $level = 0;
    }
    else
    {

        $text   = "SELECT `Level` FROM `rubrics` WHERE ID = :par_ParentID";
        $pdo = get_pdo_connection();
        $result = $pdo -> prepare($text);
        $result -> execute(array('par_ParentID' => $par_ParentID));
        $row = $result ->fetch(PDO::FETCH_LAZY);
        $level  = $row['Level'] + 1;

        $pdo = NULL;
    }

    return $level;
}

function PrintTableHeader($par_HeaderArray)
{
    echo '<tr>';
    for ($i = 0; $i <= count($par_HeaderArray) - 1; $i++)
    {
        echo '<th>'. $par_HeaderArray[$i]. '</th>';
    }
    echo '</tr>';

}

function PrintTableRow($par_RowArray, $par_type, $par_button = 0)
{
    echo '<tr>';

    foreach ($par_RowArray as $Name => $Value)
    {
        echo '  <td>'. $Value. '</td>';
        if ($Name == 'ID')
        {
            $ID = $Value;
        }
    }

    If($par_type == 'BlockList')
    {
        $table = 'rubrics';
    }
    elseif ($par_type == 'ArtList')
    {
        $table = 'posts';
    }
    elseif ($par_type == 'Users')
    {
        $table = 'users';
    }
    else
    {
        $table = 'Const';
    }

    if ($par_button == 0)
    {
        echo '<td class = "change"><a href="/myadmin/admin.php?TypeWin='. $par_type. '&ID='. $ID.'">Изменить</a></td>';
        echo '<td class = "delete"><a href="/myadmin/delete_row.php?TableName=' . $table . '&ID='. $ID.'">Удалить</a></td>';
    }
    elseif ($par_button == 1)
    {
        echo '<td class = "change"><a href="/myadmin/admin.php?TypeWin='. $par_type. '&ID='. $ID.'">ОК</a></td>';
    }

    echo '</tr>';
}

function GetBlockList($par_type)
{
    echo '<table>';

    $ColumnArray[0] = "ID";
    $ColumnArray[1] = "Наименование";
    $ColumnArray[2] = "ID Родителя";
    $ColumnArray[3] = "Уровень в иерархии";
    PrintTableHeader($ColumnArray);

    $text   =
        "SELECT "
            . "`ID`"
            . ", `Name`"
            . ", `Parent_ID`"
            . ", `Level`" .
        "FROM `rubrics`";

    $pdo = get_pdo_connection();
    $result = $pdo->query($text);
    while ($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        PrintTableRow($row, $par_type);
    }

    echo '
    <tr>
    <td class = "noborder"></td>
    <td class = "noborder"></td>
    <td class = "noborder"></td>
    <td class = "noborder"></td>';
    echo '<td class = "create"><a href="/myadmin/admin.php?TypeWin=' . $par_type . '&ID=' . '' . '">Добавить</a></td>';
    echo '</tr>';

    echo '</table>';

}

function GetArtList($par_type)
{
    $pref_url   = get_const('pref_st');

    echo '<table>';
    $ColumnArray[0] = "ID Статьи";
    $ColumnArray[1] = "Заголовок";
    $ColumnArray[2] = "ID Рубрики";
    $ColumnArray[3] = "Наименование Рубрики";

    PrintTableHeader($ColumnArray);

    $text =
    "SELECT
        posts.ID
        , posts.Header
        , pir.Rubric_ID
        , rubrics.Name
    FROM `posts` AS posts
    LEFT JOIN `post_in_rubric` AS pir ON posts.ID = pir.Post_ID
    LEFT JOIN `rubrics` AS rubrics ON pir.Rubric_ID = rubrics.ID
    WHERE posts.URL Like :par_pref";

    $pdo = get_pdo_connection();
    $result = $pdo -> prepare($text);
    $result -> execute(array('par_pref' => $pref_url . '%'));
    while ($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        PrintTableRow($row, $par_type);
    }

    echo '
    <tr>
    <td class = "noborder"></td>
    <td class = "noborder"></td>
    <td class = "noborder"></td>
    <td class = "noborder"></td>';
    echo '<td class = "create"><a href="/myadmin/admin.php?TypeWin=' . $par_type . '&ID=' . '' . '">Добавить</a></td>';
    echo '</tr>';

    echo '</table>';
}

function GetArtForm($par_ID) /*редактирование записи*/
{
    $pref_url   = get_const('pref_st');
    $pref_len   = strlen($pref_url);

    $site_name  = get_const(site_name);

    if ($par_ID !== '')
    {

        $text = '
        SELECT
            posts.Header    AS Header,
            posts.URL       AS URL,
            posts.Descr     AS Descr,
            posts.Content   AS Content,
            pir.Rubric_ID   AS RubricID,
            rubrics.Name    AS RubricName
        FROM `posts` AS posts
        LEFT JOIN `post_in_rubric` AS pir ON posts.ID = pir.Post_ID
        LEFT JOIN `rubrics` AS rubrics ON pir.Rubric_ID = rubrics.ID
        WHERE posts.ID = :par_ID';

        $pdo = get_pdo_connection();
        $result = $pdo -> prepare($text);
        $result -> execute(array('par_ID' => $par_ID));
        while ($row = $result->fetch(PDO::FETCH_LAZY))
        {
            $content        = trim($row['Content']);
            $header         = trim($row['Header']);
            $descr          = trim($row['Descr']);
            $rubric_id      = trim($row['RubricID']);
            $rubric_name    = trim($row['RubricName']);
            $url            = trim($row['URL']);
            $pref           = substr($url, 0, $pref_len);
            if ($url !== '' and $pref == $pref_url)
            {

                $url    = substr($url, $pref_len, strlen($url) - strlen($pref_url));
                $pref   = $pref_url;
            }
            else
            {
                $pref = '';
            }
        }

    }
    else
    {
        $content        = '';
        $header         = '';
        $descr          = '';
        $rubric_id      = NULL;
        $rubric_name    = '';
        $url            = '';
        $pref           = $pref_url;
    }
    $pdo = NULL;

    /*формируем список рубрик*/
    $text   = "SELECT * FROM `rubrics`";
    $text_rubric = '';

    $pdo = get_pdo_connection();
    $result = $pdo->query($text);
    while ($row = $result->fetch(PDO::FETCH_LAZY))
    {
        if($row['ID'] == $rubric_id)
        {
            $text_chek = ' checked';
        }
        else
        {
            $text_chek = '';
        }
        $text_rubric = $text_rubric .
        '<input type = "radio" name = "RubricID" value = ' . $row['ID'] . $text_chek . '>' . $row['Name'] . '<br>';
    }

    $pdo = NULL;

    echo '
    <h1>Редактирование записи</h1>
    <form method = "post" action = "change_post.php">
        <table>
            <tr>
                <td class = "noborder">ID:</td>
                <td class = "noborder"><input type = "text" name = "ID" value = "' . $par_ID . '" readonly size = "10"></td>
            </tr>
            <tr>
                <td class = "noborder">Заголовок:</td>
                <td class = "noborder"><input type = "text" size = 50 name = "Header" value = "' . $header . '"></td>
            </tr>
            <tr>
                <td class = "noborder">URL:   ' . $site_name . $pref . ' </td>
                <td class = "noborder"><input type = "text" size = 50 name = "URL" value = "' . $url . '"></td>
            </tr>
            <tr>
                <td class = "noborder">description:</td>
                <td class = "noborder"><input type = "text" size = 50 name = "Descr" value = "' . $descr . '"></td>
            </tr>
            <tr>
                <td class = "noborder" colspan = "2" align = center>Содержание:</td>
                <td class = "noborder" align = center>Рубрика:</td>
            </tr>
            <tr>
                <td class = "noborder" colspan = "2">
                    <textarea cols = 85 rows = 40 wrap = Virtual Name = "Content">' . $content . '</textarea>
                </td>
                <td class = "noborder" valign = top>' .
                    $text_rubric . '
                </td>
            </tr>
            <tr>
                <td class = "noborder" colspan = "3" align = "right"><input type = "submit" name="butonOK" value="Записать"></td>
            </tr>
        </table>
    </form>
    ';
}

function GetBlockForm($par_ID) /*редактирование рубрики*/
{
    if ($par_ID !== '')
    {

        $text   = "SELECT * FROM `rubrics` WHERE ID = :par_ID";
        $pdo = get_pdo_connection();
        $result = $pdo -> prepare($text);
        $result -> execute(array('par_ID' => $par_ID));
        while ($row = $result->fetch(PDO::FETCH_LAZY))
        {
            $block_name = $row['Name'];
            $parent_id  = $row['Parent_ID'];
        }
        $pdo = NULL;

    }
    else
    {
        $block_name = '';
        $parent_id  = NULL;
    }

    if($parent_id == 0)
    {
        $parent_id = NULL;
    }

    $text   = "SELECT * FROM `rubrics`";

    $pdo = get_pdo_connection();
    $result = $pdo->query($text);

    if($parent_id == NULL)
    {
        $text_check = ' checked';
    }
    else
    {
        $text_check = '';
    }

    $pdo = NULL;

    echo '
    <h1>Редактирование рубрики</h1>
    <form method = "post" action = "change_rubric.php">
        <table>
            <tr>
                <td class = "noborder">ID:</td>
                <td class = "noborder"><input type = "text" name = "ID" value = "' . $par_ID . '" readonly size = "10"></td>
            </tr>

            <tr>
                <td class = "noborder">Имя рубрики:</td>
                <td class = "noborder"><input type = "text" name = "BlockName" value = "' . $block_name . '" size = "70"></td>
            </tr>

            <tr>
                <td class = "noborder" valign = top>Родительская рубрика:</td>
                <td class = "noborder">
                <input type = "radio" name = "ParentID" value = NULL' . $text_check . '>Нет<br>';
                while ($row = $result->fetch(PDO::FETCH_ASSOC))
                {
                    if($row['ID'] !== $par_ID)
                    {
                        if($parent_id == $row['ID'])
                        {
                            $text_check = ' checked';
                        }
                        else
                        {
                            $text_check = '';
                        }
                        echo '<input type = "radio" name = "ParentID" value = ' . $row['ID'] . $text_check . '>' . $row['Name'] . '<br>';
                    }
                }

                $pdo = NULL;
    echo '
                </td>
            </tr>

            <tr>
                <td class = "noborder" colspan = "2" align = "right"><input type = "submit" name="butonOK" value="Записать"></td>
            </tr>

        </table>
    </form>
    ';
}

function GetConstForm()
{
    echo '
    <h2>Настройка постоянных параметров</h2>
    <form method = "post" action = "write_const.php">
    <table>
    ';

    $ColumnArray[0] = "ID";
    $ColumnArray[1] = "Имя";
    $ColumnArray[2] = "Описание";
    $ColumnArray[3] = "Значение";
    $ColumnArray[4] = "Новое Значение";
    PrintTableHeader($ColumnArray);

    $text   =
        "SELECT "
            . "`ID`"
            . ", `Name`"
            . ", `Descr`"
            . ", `Value`" .
        "FROM `Const`";

    $pdo = get_pdo_connection();
    $result = $pdo->query($text);
    while ($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        echo
        '<tr>
            <td>' . $row['ID'] . '</td>
            <td>' . $row['Name'] . '</td>
            <td>' . $row['Descr'] . '</td>
            <td><input type = "text" readonly name = ' . $row['Name'] . ' value = ' . $row['Value'] .'></td>
            <td><input type = "text" name = ' . '"new_' . $row['Name'] . '"' . '></td>
        </tr>';
    }
    echo
    '<tr>
        <td class = "noborder" colspan = "5" align = "right"><input type = "submit" name = "btOK" value = "Записать"></td>
    </tr>
    </form>';

    $pdo = NULL;
}

function GetMainForm()
{
    $ID         = '';
    $Header     = 'Главная';
    $Descr      = '';
    $Content    = '';
    $site_name  = get_const('site_name');
    $URL        = '';

    echo '
    <h2>Редактирование главной страницы</h2>
    <form method = "post" action = "change_post.php">
    <table>
    ';

    $text =
    "SELECT
        posts.ID
        , posts.Header
        , posts.Descr
        , posts.Content
    FROM `posts` AS posts
    WHERE posts.URL = ''";

    $pdo = get_pdo_connection();
    $result = $pdo->query($text);
    while ($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        $ID         = $row['ID'];
        $Header     = $row['Header'];
        $Descr      = $row['Descr'];
        $Content    = $row['Content'];
    }

    $pdo = NULL;

    echo '
        <tr>
            <td class = "noborder">ID:</td>
            <td class = "noborder"><input readonly type = "text" name = "ID" value = "' . $ID . '" size = "10"></td>
        </tr>
        <tr>
            <td class = "noborder">Заголовок:</td>
            <td class = "noborder"><input readonly type = "text" size = 84 name = "Header" value = "' . $Header . '"></td>
        </tr>
        <tr>
            <td class = "noborder">URL:   ' . $site_name . ' </td>
            <td class = "noborder"><input type = "hidden" size = 30 name = "URL" value = "' . $URL . '"></td>
        </tr>
        <tr>
            <td class = "noborder">description:</td>
            <td class = "noborder"><input type = "text" size = 84 name = "Descr" value = "' . $Descr . '"></td>
        </tr>
        <tr>
            <td class = "noborder" colspan = "2" align = center>Содержание:</td>
        </tr>
        <tr>
            <td class = "noborder" colspan = "2">
                <textarea cols = 110 rows = 40 wrap = Virtual Name = "Content">' . $Content . '</textarea>
            </td>
        </tr>
        <tr>
            <td class = "noborder" colspan = "2" align = "right"><input type = "submit" name="butonOK" value="Записать"></td>
        </tr>
    </table>
    </form>';
}

function GetUsersList($par_type)
{

    echo '<table>';
    $ColumnArray[0] = "ID Пользователя";
    $ColumnArray[1] = "Имя Пользователя";

    PrintTableHeader($ColumnArray);


    $text =
    "SELECT
        ID
        , Login
    FROM `Users` AS users";

    $pdo = get_pdo_connection();
    $result = $pdo -> query($text);
    while ($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        PrintTableRow($row, $par_type);
    }

    echo '
    <tr>
    <td class = "noborder"></td>
    <td class = "noborder"></td>';
    echo '<td class = "create"><a href="/myadmin/admin.php?TypeWin=' . $par_type . '&ID=' . '' . '">Добавить</a></td>';
    echo '</tr>';

    echo '</table>';
}

function GetUserForm($par_ID) /*редактирование пользователя*/
{
    if ($par_ID !== '')
    {

        $text   = "SELECT ID, Login FROM `users` WHERE ID = :par_ID";
        $pdo = get_pdo_connection();
        $result = $pdo -> prepare($text);
        $result -> execute(array('par_ID' => $par_ID));
        while ($row = $result->fetch(PDO::FETCH_ASSOC))
        {
            $login  = $row['Login'];
        }
        $pdo = NULL;

    }
    else
    {
        $login  = '';
    }

    echo '
    <h1>Редактирование пользователя</h1>
    <form method = "post" action = "change_user.php">
        <table>
            <tr>
                <td class = "noborder">ID:</td>
                <td class = "noborder"><input type = "text" name = "ID" value = "' . $par_ID . '" readonly size = "10"></td>
            </tr>

            <tr>
                <td class = "noborder">Логин:</td>
                <td class = "noborder"><input type = "text" name = "Login" value = "' . $login . '" size = "70"></td>
            </tr>

            <tr>
                <td class = "noborder">Пароль:</td>
                <td class = "noborder"><input type = "text" name = "Pass" value = "" size = "70"></td>
            </tr>

            <tr>
                <td class = "noborder" colspan = "2" align = "right"><input type = "submit" name="butonOK" value="Записать"></td>
            </tr>

        </table>
    </form>
    ';

}
function GetMainWindow($par_type, $par_ID)
{
    if($par_type == "ArtList")
    {
        if($par_ID !== NULL)
        {
            GetArtForm($par_ID);
        }
        else
        {
            GetArtList($par_type);
        }
    }
    elseif($par_type == "BlockList")
    {
        if($par_ID !== NULL)
        {
            GetBlockForm($par_ID);
        }
        else
        {
            GetBlockList($par_type);
        }
    }
    elseif($par_type == "Const")
    {
        GetConstForm();
    }
    elseif($par_type == "Main")
    {
        GetMainForm();
    }
    elseif($par_type == "Users")
    {
        if($par_ID !== NULL)
        {
            GetUserForm($par_ID);
        }
        else
        {
            GetUsersList($par_type);
        }
    }

}
?>
