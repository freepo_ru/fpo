<?php
require_once 'fun.php';
require_once 'const.php';
require_once 'ya_metrica.php';

if (isset($_GET['url']))
{
    $PageURL = $_GET['url'];
    if (is_null(GetRecordField($PageURL, 'ID')) and $PageURL !== 'page/content')
    {
        header("HTTP/1.0 404 Not Found");
    }
}
else
{
    $PageURL = '';
}

$tree           = GetArticleTree();
$ya_metrica     = get_metrica_text();
$site_name      = site_name;
$site_content   = site_name . 'page/content';

if ($PageURL == 'page/content')
{
    $record = $tree;
    $descr  = 'Содержание';
    $title  = 'Содержание';
}
else
{
    $tree   = GetArticleTree();
    $fields_array = GetRecordField($PageURL, array('Content', 'Descr', 'Header'));
    $descr  = $fields_array['Descr'];
    $record = $fields_array['Content'];
    $title  = $fields_array['Header'];
}

$html   =
<<<_HTML
<!DOCTYPE html>
<html lang="en">
$ya_metrica
   <head>
        <title>$title</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width">
        <meta name="yandex-verification" content="e036195cade098bf" />
        <link rel="stylesheet" type="text/css" href="/style.css">
        <meta name="description" content="$descr">'
    </head>
    <body>

        <div ID = "grid_container">
            <div ID = "gr_header">
                <h1>БЕСПЛАТНОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ</h1>
            </div>
            <div ID = "gr_sub_header">
                <ul>
                    <li><a href="$site_name">Главная</a></li>
                </ul>
            </div>
            <div id = gr_sub_header_m>
                <ul>
                    <li><a href="$site_name">Главная</a></li>
                    <li><a href="$site_content">Содержание</a></li>
                </ul>
            </div>
            <div ID = "gr_left_bar">
                <ul class="ul_rubric">
                    $tree
                </ul>
            </div>
            <div ID = "gr_main">
                $record
            </div>
            <div ID = "gr_footer">
            </div>
        </div>

    </body>

</html>
_HTML;

echo $html;

?>
