<?php
include ('db_connect.php');

function GetRecordField($param_ID, $fields) {

    if (is_array($fields))
    {
        $fields_name = '';
        foreach ($fields as $field)
        {
            $fields_name .= ($fields_name == '') ? '`' . $field . '`' : ', `' . $field . '`';
        }
    }
    else
    {
        $fields_name = '`' . $fields . '`';
    }

    $Text = "SELECT $fields_name FROM `posts` WHERE `URL` = :param_URL";
    $par_array = array('param_URL' => $param_ID,);

    $pdo = get_pdo_connection();
    $result = $pdo -> prepare($Text);
    $result -> execute($par_array);
    $row = $result ->fetch(PDO::FETCH_LAZY);
    $pdo = NULL;

    if (is_array($fields))
    {
        return $row;
    }
    else
    {
        return $row[$fields];
    }

}

function CreateTree($par_node_array, $par_parent_id)
{
    if (is_array($par_node_array) and isset($par_node_array[$par_parent_id]))
    {

        foreach ($par_node_array[$par_parent_id] as $node)
        {
            if($node[ItsPost] == 0)
            {
                if ($par_parent_id == 0)
                {
                    $tree_text .= '<li class = "li_rubric">' . $node['Name'] . '</li>';
                }
                else
                {
                    $tree_text .= '<li class = "li_rubric_1">' . $node['Name'] . '</li>';
                }
            }
            else
            {
                if($node['URL'] =='')
                {
                    $tree_text .= '<li class = "li_post_main"><a href = "' . site_name . $node['URL']. '">'  . $node['Name'] . '</a></li>';
                }
                else
                {
                    $tree_text .= '<li class = "li_post"><a href = "' . site_name . $node['URL']. '">'  . $node['Name'] . '</a></li>';
                }
            }
            $tree_text .= CreateTree($par_node_array, $node['ID']);
        }
    }

    return $tree_text;
}

function GetArticleTree()
{

    $text = "
    SELECT
        `ID`                        AS ID
        , IFNULL(`Parent_ID`, 0)    AS Parent_ID
        , `Name`                    AS Name
        , 0                         AS ItsPost
        , NULL                      AS URL
    FROM `rubrics`

    UNION ALL

    SELECT
    posts.ID                        AS ID
    , IFNULL(pin.Rubric_ID, 0)      AS Parent_ID
    , posts.Header                  AS Name
    , 1                             AS ItsPost
    , posts.URL                     AS URL
    FROM `posts` AS posts
    LEFT JOIN post_in_rubric AS pin ON posts.ID = pin.Post_ID
    WHERE posts.URL <> ''

    Order by Parent_ID, ItsPost Desc, Name";

    $tree = array();

    $pdo = get_pdo_connection();
    $result = $pdo->query($text);
    while ($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        $tree[$row['Parent_ID']][] =  $row;
    }

    return CreateTree($tree, 0);
    $pdo = NULL;
}
?>
